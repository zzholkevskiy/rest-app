Rest app
=========

## Список використаних технологій:
* PHP
* Symfony 3.4
* MySQL
* Redis
* RabbitMQ
* ElasticSearch

## Технічні нюанси:
* Для реалізації REST API використав стандартний компонент Symfony serializer з використанням груп серіалізації. 
* Як виглядає повний проце створення користувача:
    - Controller action (Етап, коли екшн обробляє необхідні дані)
        - Валідація Json schema
        - Отримання DTO з Request
        - Валідація DTO
        - Створення сутності на основі DTO
        - Валідація сутності
        - Запис DTO до Redis (використав структура даних - список)
        - Як працює обробка кастомних ексепшенів?
            - Для обробки експешнів реалізував ExceptionFormatterSubscriber, який вішається на KernelEvents::EXCEPTION.
            - У ньому за допомогою ExceptionResponseProcessor обираємо стратегію обробки помилки (ExceptionProcessor) в залежності від типу експшена.
            - У папці Service/ExceptionProcessor реалізовані обробники для кастомних помилок (помилка Json schema, помилка валідації сутності і т.д.)
    - Command
        - Створив конcольну команду для паблішингу тасок на збереження користувачів до RabbitMQ.
        - Сама таска іде з параметром count_user_to_save_per_task (скільки користувачів зберегти за один раз у тасці (зараз стоїть 100), яку буде обробляти consumer)
        - Саму консольну команду вішаємо на крон з тою частотою, яка потрібна при навантаженні (Наприклад, конжні 5 сек)
    - RabbitMQ consumer handle:
        - Отримаємо параметр count_user_to_save_per_task
        - Далі за допомогою методу rpop отримуємо користувачів зі списку redis.
        - Зберігаємо нового користувача
* Для пошуку використав ElasticSearch
* Написав для прикладу тест - UserManagerTest.php (composer ci:phpunit)
* Додав перевірку на код стайл (Codesniffer, FriendsOfPHP/PHP-CS-Fixer) (composer ci:code-style)
* Додав статичний аналізатор - phpstan (composer ci:static-analysis)

## Встановлення:
```
1. composer install
2. composer app:recreate-database  // Створення БД, оновлення схеми БД, завантаження фікстур
3. php bin/console server:run
```

