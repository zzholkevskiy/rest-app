<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\Manager\User\UserRedisManager;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SaveNewUserProducerCommand.
 */
class SaveNewUserProducerCommand extends Command
{
    const COUNT_USER_TO_SAVE_PER_TASK = 100;

    /** @var ProducerInterface */
    private $saveNewUserProducer;

    /** @var UserRedisManager */
    private $userRedisManager;

    /**
     * SaveNewUserProducerCommand constructor.
     *
     * @param ProducerInterface $saveNewUserProducer
     * @param UserRedisManager  $userRedisManager
     */
    public function __construct(ProducerInterface $saveNewUserProducer, UserRedisManager $userRedisManager)
    {
        parent::__construct();

        $this->saveNewUserProducer = $saveNewUserProducer;
        $this->userRedisManager = $userRedisManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('app:save-new-user-producer')
            ->setDescription('Publish new task to save new user');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $this->saveNewUserProducer->publish(json_encode(['count_user_to_save_per_task' => self::COUNT_USER_TO_SAVE_PER_TASK]));
    }
}
