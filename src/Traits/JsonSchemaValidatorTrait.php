<?php

declare(strict_types=1);

namespace App\Traits;

use App\Service\Validation\JsonSchemaValidator;

/**
 * Trait JsonSchemaValidatorTrait.
 */
trait JsonSchemaValidatorTrait
{
    /** @var JsonSchemaValidator */
    protected $jsonSchemaValidator;

    /**
     * @param JsonSchemaValidator $jsonSchemaValidator
     *
     * @required
     */
    public function setJsonSchemaValidator(JsonSchemaValidator $jsonSchemaValidator): void
    {
        $this->jsonSchemaValidator = $jsonSchemaValidator;
    }
}
