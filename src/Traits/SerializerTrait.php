<?php

declare(strict_types=1);

namespace App\Traits;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Trait SerializerTrait.
 */
trait SerializerTrait
{
    /** @var Serializer|SerializerInterface */
    protected $serializer;

    /**
     * @param SerializerInterface $serializer
     *
     * @required
     */
    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }
}
