<?php

declare(strict_types=1);

namespace App\Controller\API\Core;

use App\DTO\Core\DTOInterface;
use App\Entity\EntityInterface;
use App\Exception\Http\InvalidEntityException;
use App\Traits\JsonSchemaValidatorTrait;
use App\Traits\SerializerTrait;
use App\Traits\ValidatorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraint;

/**
 * Class AbstractDTOBasedAction.
 */
abstract class AbstractDTOBasedAction
{
    use JsonSchemaValidatorTrait;
    use SerializerTrait;
    use ValidatorTrait;

    /**
     * @param Request $request
     * @param string  $jsonSchemeFilePath
     */
    protected function validateJsonSchema(Request $request, string $jsonSchemeFilePath): void
    {
        $this->jsonSchemaValidator->validateRequest($request, $jsonSchemeFilePath);
    }

    /**
     * @param DTOInterface                 $dto
     * @param Constraint|Constraint[]|null $constraints
     * @param array|null                   $groups
     */
    protected function validateDto(DTOInterface $dto, $constraints = null, array $groups = null): void
    {
        $this->validate($dto, $constraints, $groups);
    }

    /**
     * @param mixed                        $entity
     * @param Constraint|Constraint[]|null $constraints
     * @param array|null                   $groups
     */
    protected function validateEntity($entity, $constraints = null, array $groups = null): void
    {
        $this->validate($entity, $constraints, $groups);
    }

    /**
     * @param Request $request
     * @param string  $dtoClassName
     *
     * @return object|DTOInterface
     */
    protected function getDtoFromRequest(Request $request, string $dtoClassName)
    {
        return $this->serializer->deserialize($request->getContent(), $dtoClassName, 'json');
    }

    /**
     * @param DTOInterface|EntityInterface $entity
     * @param null                         $constraints
     * @param array|null                   $groups
     */
    private function validate($entity, $constraints = null, array $groups = null): void
    {
        $errors = $this->validator->validate($entity, $constraints, $groups);
        if (\count($errors) > 0) {
            throw new InvalidEntityException($errors);
        }
    }
}
