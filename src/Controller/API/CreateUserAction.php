<?php

declare(strict_types=1);

namespace App\Controller\API;

use App\Controller\API\Core\AbstractDTOBasedAction;
use App\DTO\User\CreateUserDTO;
use App\Service\Manager\User\UserManager;
use App\Service\Manager\User\UserRedisManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CreateUserAction.
 */
class CreateUserAction extends AbstractDTOBasedAction
{
    /** @var UserManager */
    private $userManager;

    /** @var UserRedisManager */
    private $userRedisManager;

    /**
     * CreateUserAction constructor.
     *
     * @param UserManager      $userManager
     * @param UserRedisManager $userRedisManager
     */
    public function __construct(UserManager $userManager, UserRedisManager $userRedisManager)
    {
        $this->userManager = $userManager;
        $this->userRedisManager = $userRedisManager;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \Exception
     *
     * @Route("/users", name="api.v1.0.users.create",  methods={"POST"})
     */
    public function executeAction(Request $request): JsonResponse
    {
        $this->validateJsonSchema($request, CreateUserDTO::getJsonSchemeFilePath());

        /** @var CreateUserDTO $dto */
        $dto = $this->getDtoFromRequest($request, CreateUserDTO::class);
        $this->validateDto($dto);

        $user = $this->userManager->createUserFromDTO($dto);

        $this->validateEntity($user);

        $this->userRedisManager->saveUserCreateRecordToRedis($dto);

        $data = $this->serializer->serialize($dto, 'json', ['groups' => ['users.create']]);

        return new JsonResponse($data, JsonResponse::HTTP_CREATED, [], true);
    }
}
