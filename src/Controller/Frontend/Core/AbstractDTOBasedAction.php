<?php

declare(strict_types=1);

namespace App\Controller\Frontend\Core;

use App\DTO\Core\DTOInterface;
use App\Traits\SerializerTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AbstractDTOBasedAction.
 */
abstract class AbstractDTOBasedAction
{
    use SerializerTrait;

    /**
     * @param Request $request
     * @param string  $dtoClassName
     *
     * @return DTOInterface|object
     */
    protected function getDtoFromRequest(Request $request, string $dtoClassName)
    {
        return $this->serializer->deserialize(json_encode($request->query->all()), $dtoClassName, 'json');
    }
}
