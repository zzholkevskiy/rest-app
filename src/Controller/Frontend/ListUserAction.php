<?php

declare(strict_types=1);

namespace App\Controller\Frontend;

use App\Controller\Frontend\Core\AbstractDTOBasedAction;
use App\DTO\User\UserSearchDTO;
use App\Service\Manager\User\UserSearchManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

/**
 * Class ListUserAction.
 */
class ListUserAction extends AbstractDTOBasedAction
{
    /** @var UserSearchManager */
    private $userSearchManager;

    /** @var Environment */
    private $twig;

    /**
     * ListUserAction constructor.
     *
     * @param UserSearchManager $userSearchManager
     * @param Environment       $twig
     */
    public function __construct(UserSearchManager $userSearchManager, Environment $twig)
    {
        $this->userSearchManager = $userSearchManager;
        $this->twig = $twig;
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     *
     * @Route("/users/list", name="frontend.users_list")
     */
    public function executeAction(Request $request): Response
    {
        /** @var UserSearchDTO $dto */
        $dto = $this->getDtoFromRequest($request, UserSearchDTO::class);

        $pagination = $this->userSearchManager->find(
            $dto,
            $request->query->getInt('page', 1)
        );

        return new Response(
            $this->twig->render(
                'users/list.html.twig',
                [
                    'pagination' => $pagination,
                ]
            )
        );
    }
}
