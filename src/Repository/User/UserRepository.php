<?php

declare(strict_types=1);

namespace App\Repository\User;

use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository.
 */
class UserRepository extends EntityRepository
{
}
