<?php

declare(strict_types=1);

namespace App\Serializer\Normalizer;

use JsonSchema\Validator as JsonSchemaValidator;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * JsonSchemaErrorNormalizer.
 */
class JsonSchemaErrorNormalizer implements NormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = []): array
    {
        $data = [];

        /** @var JsonSchemaValidator $object */
        foreach ($object->getErrors() as $error) {
            $data[$error['constraint']][] = [
                $error['property'] => $error['message'],
            ];
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof JsonSchemaValidator;
    }
}
