<?php

declare(strict_types=1);

namespace App\Serializer\Normalizer\DTO\User;

use App\DTO\User\CreateUserDTO;
use App\DTO\User\CreateUserNumberDTO;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class CreateUserDTONormalizer.
 */
class CreateUserDTONormalizer implements DenormalizerInterface
{
    /** @var ObjectNormalizer */
    private $objectNormalizer;

    /**
     * CreateUserDTONormalizer constructor.
     *
     * @param ObjectNormalizer $objectNormalizer
     */
    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = []): CreateUserDTO
    {
        /** @var CreateUserDTO $dto */
        $dto = $this->objectNormalizer->denormalize($data, $class, $format, $context);
        if (\array_key_exists('phone_numbers', $data) && \is_array($data['phone_numbers'])) {
            foreach ($data['phone_numbers'] as $phoneNumber) {
                $dto->addPhoneNumber(
                    (new CreateUserNumberDTO())
                        ->setNumber(\is_array($phoneNumber) ? $phoneNumber['number'] : $phoneNumber)
                );
            }
        }

        return $dto;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return CreateUserDTO::class === $type;
    }
}
