<?php

declare(strict_types=1);

namespace App\Serializer\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class ConstraintViolationListNormalizer.
 */
class ConstraintViolationListNormalizer implements NormalizerInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws \RuntimeException
     */
    public function normalize($object, $format = null, array $context = []): array
    {
        $data = [];

        /** @var ConstraintViolation $violation */
        foreach ($object as $violation) {
            $data[] = [
                'field' => $violation->getPropertyPath(),
                'message' => $violation->getMessage(),
            ];
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof ConstraintViolationList;
    }
}
