<?php

declare(strict_types=1);

namespace App\Consumer;

use App\DTO\User\CreateUserDTO;
use App\Service\Manager\User\UserManager;
use App\Service\Manager\User\UserRedisManager;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class SaveNewUserConsumer.
 */
class SaveNewUserConsumer implements ConsumerInterface
{
    /** @var UserManager */
    private $userManager;

    /** @var UserRedisManager */
    private $userRedisManager;

    /** @var SerializerInterface */
    private $serializer;

    /**
     * SaveNewUserConsumer constructor.
     *
     * @param UserManager         $userManager
     * @param UserRedisManager    $userRedisManager
     * @param SerializerInterface $serializer
     */
    public function __construct(UserManager $userManager, UserRedisManager $userRedisManager, SerializerInterface $serializer)
    {
        $this->userManager = $userManager;
        $this->userRedisManager = $userRedisManager;
        $this->serializer = $serializer;

        \gc_enable();
    }

    /**
     * {@inheritdoc}
     */
    public function execute(AMQPMessage $msg): int
    {
        $data = \json_decode($msg->getBody(), true);
        if (!\array_key_exists('count_user_to_save_per_task', $data)) {
            \gc_collect_cycles();

            return ConsumerInterface::MSG_REJECT;
        }

        $countUserToSavePerTask = $data['count_user_to_save_per_task'];
        for ($i = 0; $i < $countUserToSavePerTask; ++$i) {
            $record = $this->userRedisManager->getUserCreateRecord();
            if (null === $record) {
                break;
            }

            /** @var CreateUserDTO $userDTO */
            $userDTO = $this->serializer->deserialize($record, CreateUserDTO::class, 'json');

            $user = $this->userManager->createUserFromDTO($userDTO);
            $this->userManager->save($user);
        }

        \gc_collect_cycles();

        return ConsumerInterface::MSG_ACK;
    }
}
