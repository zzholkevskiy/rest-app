<?php

declare(strict_types=1);

namespace App\DataFixtures\ORM;

use App\Entity\User\User;
use App\Entity\User\UserPhoneNumber;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

/**
 * Class LoadUserData.
 */
class LoadUserData extends Fixture
{
    public const COUNT_USERS = 100;

    /** @var \Faker\Generator $faker */
    private $faker;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->faker = Factory::create('uk_UA');
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < self::COUNT_USERS; ++$i) {
            $user = (new User())
                ->setFirstName($this->faker->firstName)
                ->setLastName($this->faker->lastName)
                ->addPhoneNumber(
                    (new UserPhoneNumber())
                        ->setNumber($this->faker->phoneNumber)
                )
                ->addPhoneNumber(
                    (new UserPhoneNumber())
                        ->setNumber($this->faker->phoneNumber)
                );

            $manager->persist($user);
        }

        $manager->flush();
    }
}
