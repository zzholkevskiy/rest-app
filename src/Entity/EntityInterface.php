<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * Interface EntityInterface.
 */
interface EntityInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;
}
