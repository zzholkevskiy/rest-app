<?php

declare(strict_types=1);

namespace App\Entity\User;

use App\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User.
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\User\UserRepository")
 */
class User implements EntityInterface
{
    use TimestampableEntity;

    /**
     * @var int ID
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection|UserPhoneNumber[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\UserPhoneNumber", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @Assert\Count(min=1)
     */
    private $phoneNumbers;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     */
    private $lastName;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->phoneNumbers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return UserPhoneNumber[]|ArrayCollection
     */
    public function getPhoneNumbers(): ArrayCollection
    {
        return $this->phoneNumbers;
    }

    /**
     * @param UserPhoneNumber[]|ArrayCollection $phoneNumbers
     *
     * @return User
     */
    public function setPhoneNumbers(ArrayCollection $phoneNumbers): self
    {
        $this->phoneNumbers = $phoneNumbers;

        return $this;
    }

    /**
     * @param UserPhoneNumber $userPhoneNumber
     *
     * @return $this
     */
    public function addPhoneNumber(UserPhoneNumber $userPhoneNumber): self
    {
        if (!$this->phoneNumbers->contains($userPhoneNumber)) {
            $userPhoneNumber->setUser($this);
            $this->phoneNumbers->add($userPhoneNumber);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }
}
