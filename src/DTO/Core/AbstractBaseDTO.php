<?php

declare(strict_types=1);

namespace App\DTO\Core;

/**
 * Class AbstractBaseDTO.
 */
abstract class AbstractBaseDTO
{
    private const DIR_TO_JSON_SCHEMA_FILES = __DIR__.'/../../Json/Schema/';

    /**
     * @return string
     */
    abstract public static function jsonSchemeName(): string;

    /**
     * @return string
     */
    public static function getJsonSchemeFilePath(): string
    {
        return sprintf('%s%s.json', static::getJsonSchemeBasePath(), static::jsonSchemeName());
    }

    /**
     * @return string
     */
    private static function getJsonSchemeBasePath(): string
    {
        return self::DIR_TO_JSON_SCHEMA_FILES;
    }
}
