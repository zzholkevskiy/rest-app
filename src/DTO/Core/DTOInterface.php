<?php

declare(strict_types=1);

namespace App\DTO\Core;

/**
 * Interface DTOInterface.
 */
interface DTOInterface
{
    /**
     * @return string
     */
    public static function jsonSchemeName(): string;
}
