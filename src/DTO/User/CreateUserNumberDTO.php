<?php

declare(strict_types=1);

namespace App\DTO\User;

use App\DTO\Core\DTOInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CreateUserNumberDTO.
 */
class CreateUserNumberDTO implements DTOInterface
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min=10, max=14)
     *
     * @Groups({
     *     "users.create",
     * })
     */
    private $number;

    /**
     * {@inheritdoc}
     */
    public static function jsonSchemeName(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param string $number
     *
     * @return CreateUserNumberDTO
     */
    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }
}
