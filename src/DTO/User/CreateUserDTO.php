<?php

declare(strict_types=1);

namespace App\DTO\User;

use App\DTO\Core\AbstractBaseDTO;
use App\DTO\Core\DTOInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CreateUserDTO.
 */
class CreateUserDTO extends AbstractBaseDTO implements DTOInterface
{
    /**
     * @var ArrayCollection|CreateUserNumberDTO[]
     *
     * @Groups({
     *     "users.create",
     * })
     *
     * @Assert\Count(min=1)
     */
    private $phoneNumbers;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     *
     * @Groups({
     *     "users.create",
     * })
     */
    private $firstName;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     *
     * @Groups({
     *     "users.create",
     * })
     */
    private $lastName;

    /**
     * CreateUserDTO constructor.
     */
    public function __construct()
    {
        $this->phoneNumbers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public static function jsonSchemeName(): string
    {
        return 'NewUser';
    }

    /**
     * @return CreateUserNumberDTO[]|ArrayCollection
     */
    public function getPhoneNumbers(): ArrayCollection
    {
        return $this->phoneNumbers;
    }

    /**
     * @param CreateUserNumberDTO[]|ArrayCollection $phoneNumbers
     *
     * @return CreateUserDTO
     */
    public function setPhoneNumbers(ArrayCollection $phoneNumbers): self
    {
        $this->phoneNumbers = $phoneNumbers;

        return $this;
    }

    /**
     * @param CreateUserNumberDTO $userPhoneNumber
     *
     * @return $this
     */
    public function addPhoneNumber(CreateUserNumberDTO $userPhoneNumber): self
    {
        if (!$this->phoneNumbers->contains($userPhoneNumber)) {
            $this->phoneNumbers->add($userPhoneNumber);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return CreateUserDTO
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return CreateUserDTO
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }
}
