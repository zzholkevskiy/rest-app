<?php

declare(strict_types=1);

namespace App\DTO\User;

use App\DTO\Core\AbstractBaseDTO;
use App\DTO\Core\DTOInterface;

/**
 * Class UserSearchDTO.
 */
class UserSearchDTO extends AbstractBaseDTO implements DTOInterface
{
    /** @var string|null */
    private $firstName;

    /** @var string|null */
    private $lastName;

    /** @var string|null */
    private $firstNameSort;

    /**
     * @return string
     */
    public static function jsonSchemeName(): string
    {
        return 'UserSearch';
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return UserSearchDTO
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return UserSearchDTO
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstNameSort(): ?string
    {
        return $this->firstNameSort;
    }

    /**
     * @param string $firstNameSort
     *
     * @return UserSearchDTO
     */
    public function setFirstNameSort(string $firstNameSort): self
    {
        $this->firstNameSort = $firstNameSort;

        return $this;
    }
}
