<?php

declare(strict_types=1);

namespace App\Service\ExceptionProcessor;

use App\Exception\CustomExceptionInterface;
use App\Exception\Http\InvalidJsonSchemaException;

/**
 * Class InvalidJsonSchemaExceptionProcessor.
 */
class InvalidJsonSchemaExceptionProcessor implements CustomExceptionProcessorInterface
{
    /**
     * {@inheritdoc}
     */
    public function supports(CustomExceptionInterface $exception): bool
    {
        return $exception instanceof InvalidJsonSchemaException;
    }

    /**
     * {@inheritdoc}
     */
    public function processResponse(CustomExceptionInterface $exception): array
    {
        return ['violations' => $exception->getViolations()];
    }
}
