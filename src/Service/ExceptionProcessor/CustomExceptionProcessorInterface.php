<?php

declare(strict_types=1);

namespace App\Service\ExceptionProcessor;

use App\Exception\CustomExceptionInterface;

/**
 * Interface CustomExceptionProcessorInterface.
 */
interface CustomExceptionProcessorInterface
{
    /**
     * @param CustomExceptionInterface $exception
     *
     * @return bool
     */
    public function supports(CustomExceptionInterface $exception): bool;

    /**
     * @param CustomExceptionInterface $exception
     *
     * @return array
     */
    public function processResponse(CustomExceptionInterface $exception): array;
}
