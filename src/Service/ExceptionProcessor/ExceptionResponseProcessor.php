<?php

declare(strict_types=1);

namespace App\Service\ExceptionProcessor;

use App\Exception\CustomExceptionInterface;

/**
 * Class ExceptionResponseProcessor.
 */
class ExceptionResponseProcessor
{
    /** @var iterable */
    private $exceptionProcessors;

    /**
     * @param iterable $exceptionProcessors
     */
    public function __construct(iterable $exceptionProcessors)
    {
        $this->exceptionProcessors = $exceptionProcessors;
    }

    /**
     * @param CustomExceptionInterface $exception
     *
     * @return array
     */
    public function processResponseOnException(CustomExceptionInterface $exception): array
    {
        /** @var CustomExceptionProcessorInterface $exceptionProcessor */
        foreach ($this->exceptionProcessors as $exceptionProcessor) {
            if ($exceptionProcessor->supports($exception)) {
                return $exceptionProcessor->processResponse($exception);
            }
        }

        return [];
    }
}
