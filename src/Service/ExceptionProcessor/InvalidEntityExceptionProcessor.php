<?php

declare(strict_types=1);

namespace App\Service\ExceptionProcessor;

use App\Exception\CustomExceptionInterface;
use App\Exception\Http\InvalidEntityException;

/**
 * Class InvalidEntityExceptionProcessor.
 */
class InvalidEntityExceptionProcessor implements CustomExceptionProcessorInterface
{
    /**
     * {@inheritdoc}
     */
    public function supports(CustomExceptionInterface $exception): bool
    {
        return $exception instanceof InvalidEntityException;
    }

    /**
     * {@inheritdoc}
     */
    public function processResponse(CustomExceptionInterface $exception): array
    {
        return ['violations' => $exception->getErrors()];
    }
}
