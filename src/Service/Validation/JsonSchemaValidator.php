<?php

declare(strict_types=1);

namespace App\Service\Validation;

use App\Exception\Http\InvalidJsonException;
use App\Exception\Http\InvalidJsonSchemaException;
use App\Traits\SerializerTrait;
use JsonSchema\Constraints\Constraint;
use JsonSchema\Validator;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class JsonSchemaValidator.
 */
class JsonSchemaValidator
{
    use SerializerTrait;

    /** @var Validator */
    private $validator;

    /**
     * JsonSchemaValidator constructor.
     *
     * @param Validator $validator
     */
    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @param string  $jsonSchemaFilePath
     */
    public function validateRequest(Request $request, string $jsonSchemaFilePath): void
    {
        $data = $this->decodeJsonFromRequest($request);
        $jsonSchema = \json_decode(\file_get_contents($jsonSchemaFilePath), true);

        $this->validator->validate($data, $jsonSchema, Constraint::CHECK_MODE_NORMAL);

        if (!$this->validator->isValid()) {
            $violations = $this->serializer->normalize($this->validator, 'json', ['json_schema' => $jsonSchemaFilePath]);
            $normalizedJsonSchema = $this->serializer->normalize($jsonSchemaFilePath, 'object');

            throw new InvalidJsonSchemaException($violations, $normalizedJsonSchema);
        }
    }

    /**
     * @param Request $request
     *
     * @return mixed
     *
     * @throws InvalidJsonException
     */
    private function decodeJsonFromRequest(Request $request)
    {
        $data = \json_decode($request->getContent());

        if (null === $data) {
            throw new InvalidJsonException(\sprintf('Request is not a valid JSON. Error: %s', \json_last_error_msg()));
        }

        return $data;
    }
}
