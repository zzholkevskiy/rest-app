<?php

declare(strict_types=1);

namespace App\Service\Manager\Core;

use App\Entity\EntityInterface;
use App\Traits\EntityManagerTrait;
use Doctrine\ORM\EntityRepository;

/**
 * Class AbstractBaseManager.
 */
abstract class AbstractBaseManager
{
    use EntityManagerTrait;

    /** @var EntityRepository */
    protected $repository;

    /**
     * @param EntityInterface $entity
     */
    public function save(EntityInterface $entity): void
    {
        if (null === $entity->getId()) {
            $this->em->persist($entity);
        }

        $this->em->flush();
    }

    /**
     * @param int      $id
     * @param int|null $lockMode
     * @param int|null $lockVersion
     *
     * @return object|null
     */
    public function find(int $id, ?int $lockMode = null, ?int $lockVersion = null)
    {
        return $this->repository->find($id, $lockMode, $lockVersion);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int        $limit
     * @param int        $offset
     *
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = null, int $limit = 10, int $offset = 0)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param EntityInterface $entity
     */
    public function remove(EntityInterface $entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }
}
