<?php

declare(strict_types=1);

namespace App\Service\Manager\User;

use App\DTO\User\CreateUserDTO;
use App\Entity\User\User;
use App\Entity\User\UserPhoneNumber;
use App\Service\Manager\Core\AbstractBaseManager;

/**
 * Class UserManager.
 */
class UserManager extends AbstractBaseManager
{
    /**
     * @param CreateUserDTO $dto
     *
     * @return User
     */
    public function createUserFromDTO(CreateUserDTO $dto): User
    {
        $user = (new User())
            ->setFirstName($dto->getFirstName())
            ->setLastName($dto->getLastName())
        ;

        /** @var UserPhoneNumber $phoneNumber */
        foreach ($dto->getPhoneNumbers() as $phoneNumber) {
            $user->addPhoneNumber(
                (new UserPhoneNumber())
                    ->setNumber($phoneNumber->getNumber())
            );
        }

        return $user;
    }
}
