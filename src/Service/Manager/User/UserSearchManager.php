<?php

declare(strict_types=1);

namespace App\Service\Manager\User;

use App\DTO\User\UserSearchDTO;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * Class UserSearchManager.
 */
class UserSearchManager
{
    /** @var TransformedFinder */
    private $userFinder;

    /** @var PaginatorInterface */
    private $paginator;

    /**
     * UserSearchManager constructor.
     *
     * @param TransformedFinder  $userFinder
     * @param PaginatorInterface $paginator
     */
    public function __construct(TransformedFinder $userFinder, PaginatorInterface $paginator)
    {
        $this->userFinder = $userFinder;
        $this->paginator = $paginator;
    }

    /**
     * @param UserSearchDTO $dto
     * @param int           $page
     * @param int           $countProductPerPage
     *
     * @return PaginationInterface
     */
    public function find(UserSearchDTO $dto, $page = 1, $countProductPerPage = 10): PaginationInterface
    {
        $boolQuery = new BoolQuery();

        if (!empty($dto->getFirstName())) {
            $boolQuery->addShould($this->getFirstNameMatch($dto->getFirstName()));
        }

        if (!empty($dto->getLastName())) {
            $boolQuery->addShould($this->getLastNameMatch($dto->getLastName()));
        }

        $query = new Query();
        $sortByPrice = $this->getSortByFirstName($dto->getFirstNameSort());
        if (null !== $sortByPrice) {
            $query->addSort($sortByPrice);
        }
        $query->setQuery($boolQuery);

        $results = $this->userFinder->createPaginatorAdapter($query);

        return $this->paginator->paginate($results, $page, $countProductPerPage);
    }

    /**
     * @param string|null $firstName
     *
     * @return Match
     */
    protected function getFirstNameMatch(?string $firstName): Match
    {
        $nameQuery = new Match();
        $nameQuery->setFieldQuery('first_name', $firstName);

        return $nameQuery;
    }

    /**
     * @param string|null $lastName
     *
     * @return Match
     */
    protected function getLastNameMatch(?string $lastName): Match
    {
        $nameQuery = new Match();
        $nameQuery->setFieldQuery('last_name', $lastName);

        return $nameQuery;
    }

    /**
     * @param string|null $firstNameSort
     *
     * @return array|null
     */
    protected function getSortByFirstName(?string $firstNameSort): ?array
    {
        if ('ASC' === $firstNameSort || 'DESC' === $firstNameSort) {
            return [
                'first_name' => [
                    'order' => $firstNameSort,
                ],
            ];
        }

        return null;
    }
}
