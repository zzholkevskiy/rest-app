<?php

declare(strict_types=1);

namespace App\Service\Manager\User;

use App\DTO\User\CreateUserDTO;
use Predis\Client;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class UserRedisManager.
 */
class UserRedisManager
{
    public const USER_CREATE_REDIS_LIST = 'users.create';

    /** @var Client */
    protected $redisClient;

    /** @var SerializerInterface */
    protected $serializer;

    /**
     * UserManager constructor.
     *
     * @param Client              $redisClient
     * @param SerializerInterface $serializer
     */
    public function __construct(Client $redisClient, SerializerInterface $serializer)
    {
        $this->redisClient = $redisClient;
        $this->serializer = $serializer;
    }

    /**
     * @param CreateUserDTO $dto
     */
    public function saveUserCreateRecordToRedis(CreateUserDTO $dto): void
    {
        $this->redisClient->lpush(self::USER_CREATE_REDIS_LIST, [$this->serializer->serialize($dto, 'json')]);
    }

    /**
     * @return string
     */
    public function getUserCreateRecord(): ?string
    {
        return $this->redisClient->rpop(self::USER_CREATE_REDIS_LIST);
    }
}
