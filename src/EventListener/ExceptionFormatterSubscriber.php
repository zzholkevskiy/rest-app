<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Exception\CustomExceptionInterface;
use App\Service\ExceptionProcessor\ExceptionResponseProcessor;
use App\Traits\SerializerTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ExceptionFormatterSubscriber.
 */
class ExceptionFormatterSubscriber implements EventSubscriberInterface
{
    use SerializerTrait;

    /** @var ExceptionResponseProcessor */
    private $exceptionResponseProcessor;

    /**
     * ExceptionFormatterSubscriber constructor.
     *
     * @param ExceptionResponseProcessor $exceptionResponseProcessor
     */
    public function __construct(ExceptionResponseProcessor $exceptionResponseProcessor)
    {
        $this->exceptionResponseProcessor = $exceptionResponseProcessor;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $exception = $event->getException();
        if ($exception instanceof HttpExceptionInterface) {
            $statusCode = $exception->getStatusCode();
        } else {
            $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        $responseData = [
            'error' => $exception->getMessage(),
        ];

        if ($exception instanceof CustomExceptionInterface) {
            $exceptionResponse = $this->exceptionResponseProcessor->processResponseOnException($exception);
            $responseData = \array_merge($responseData, $exceptionResponse);
        }

        $json = $this->serializer->serialize($responseData, 'json');

        $response = (new JsonResponse($json, $statusCode, [], true))->setEncodingOptions(\JSON_UNESCAPED_SLASHES);

        $event->setResponse($response);
    }
}
