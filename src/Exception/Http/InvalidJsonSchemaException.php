<?php

declare(strict_types=1);

namespace App\Exception\Http;

use App\Exception\CustomExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class InvalidJsonSchemaException.
 */
class InvalidJsonSchemaException extends HttpException implements CustomExceptionInterface
{
    /** @var array */
    private $violations;

    /** @var array */
    private $jsonSchema;

    /**
     * {@inheritdoc}
     */
    public function __construct($violations, $jsonSchema, \Exception $previous = null)
    {
        $this->violations = $violations;
        $this->jsonSchema = $jsonSchema;

        parent::__construct(Response::HTTP_BAD_REQUEST, 'Request does not match JSON Schema.', $previous);
    }

    /**
     * @return array
     */
    public function getViolations()
    {
        return $this->violations;
    }

    /**
     * @return array
     */
    public function getJsonSchema()
    {
        return $this->jsonSchema;
    }
}
