<?php

declare(strict_types=1);

namespace App\Exception\Http;

use App\Exception\CustomExceptionInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class InvalidJsonException.
 */
class InvalidJsonException extends HttpException implements CustomExceptionInterface
{
}
