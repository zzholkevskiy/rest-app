<?php

declare(strict_types=1);

namespace App\Exception\Http;

use App\Exception\CustomExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class InvalidEntityException.
 */
class InvalidEntityException extends HttpException implements CustomExceptionInterface
{
    /** @var ConstraintViolationListInterface $errors */
    private $errors;

    /**
     * {@inheritdoc}
     */
    public function __construct(ConstraintViolationListInterface $errors, \Exception $previous = null)
    {
        $this->errors = $errors;

        parent::__construct(Response::HTTP_UNPROCESSABLE_ENTITY, 'Entity has validation errors', $previous);
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getErrors(): ConstraintViolationListInterface
    {
        return $this->errors;
    }
}
