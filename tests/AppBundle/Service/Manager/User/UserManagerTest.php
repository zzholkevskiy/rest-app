<?php

namespace App\Tests\AppBundle\Service\Manager\User;

use App\DTO\User\CreateUserDTO;
use App\DTO\User\CreateUserNumberDTO;
use App\Entity\User\User;
use App\Service\Manager\User\UserManager;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

/**
 * Class UserManagerTest.
 */
class UserManagerTest extends TestCase
{
    /** @var UserManager */
    private $userManager;

    /** @var EntityManager */
    private $em;

    protected function setUp(): void
    {
        $this->em = $this->createMock(EntityManager::class);
        $this->userManager = new UserManager();
        $this->userManager->setEntityManager($this->em);
    }

    protected function tearDown(): void
    {
        unset(
            $this->em,
            $this->userManager
        );
    }

    public function testCreateUserFromDTO(): void
    {
        $userDto = (new CreateUserDTO())
            ->setFirstName('Ivanov')
            ->setLastName('Ivan')
            ->addPhoneNumber(
                (new CreateUserNumberDTO())
                    ->setNumber('+380976543211')
            )
            ->addPhoneNumber(
                (new CreateUserNumberDTO())
                    ->setNumber('+380976543211')
            );

        $userEntity = $this->userManager->createUserFromDTO($userDto);

        self::assertInstanceOf(User::class, $userEntity);
        self::assertEquals($userDto->getFirstName(), $userEntity->getFirstName());
        self::assertEquals($userDto->getLastName(), $userEntity->getLastName());
        self::assertEquals(2, $userEntity->getPhoneNumbers()->count());
        self::assertEquals(
            $userDto->getPhoneNumbers()->first()->getNumber(),
            $userEntity->getPhoneNumbers()->first()->getNumber()
        );
    }
}
